package cdt;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Pile extends Observable{
	
	private List<Integer> list;
	
	public Pile(){
		list = new ArrayList<Integer>();
		ViewBottomPile vbp = new ViewBottomPile();
		this.addObserver(vbp);
		ViewTopPile vtp = new ViewTopPile();
		this.addObserver(vtp);
	}
	
	public Integer getSizeList(){
		return this.list.size();
	}
	
	public Integer getEntier(int index){
		return this.list.get(index);
	}
	
	public void push(int entier){
		list.add(entier);
		this.setChanged();
		this.notifyObservers(getSizeList());
	}
	
	public Integer pop(){
		int value = list.get(this.getSizeList() - 1);
		list.remove(this.getSizeList() - 1);
		this.setChanged();
		this.notifyObservers(getSizeList());
		return value;
	}
	
	public void clear(){
		this.list.clear();
		this.setChanged();
		this.notifyObservers(getSizeList());
	}
}