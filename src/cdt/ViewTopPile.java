package cdt;

import java.util.Observable;
import java.util.Observer;

public class ViewTopPile implements Observer{
	
	@Override
	public void update(Observable o, Object arg) {
		int i = (int) arg;
		if(i == 0){
			System.out.println("Pile est null!");
		}
		else
			System.out.println(((Pile)o).getEntier(((Pile)o).getSizeList() - 1));
	}
}