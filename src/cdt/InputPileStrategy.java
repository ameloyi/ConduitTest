package cdt;

public abstract class InputPileStrategy {
	Pile pile;
	public InputPileStrategy(){
		this.pile = new Pile();
	}
	
	public void push(int entier){
		this.pile.push(entier);
	}
	
	public Integer pop(){
		return this.pile.pop();
	}
	
	public void clear(){
		this.pile.clear();
	}
	
	public void actionCommande(){}
}
